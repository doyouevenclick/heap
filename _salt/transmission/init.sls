transmission-daemon:
  pkg.installed: []
  service.running:
    - enable: true
    - reload: true
    - listen:
      - file: /etc/transmission-daemon/settings.json
      - file: /mnt/stor/downloads

/mnt/stor/downloads:
  file.directory:
    - mode: "0777"

/etc/transmission-daemon/settings.json:
  file.serialize:
    - formatter: json
    - dataset:
        download-dir: /mnt/stor/downloads
        download-limit: 100
        download-limit-enabled: 0
        incomplete-dir: /var/lib/transmission-daemon/downloads
        incomplete-dir-enabled: true
        preallocation: 1

        encryption: 2
        max-peers-global: 200
        peer-port: 55198,
        peer-port-random-high: 65535
        peer-port-random-low: 49152
        peer-port-random-on-start: true
        port-forwarding-enabled: 1
        pex-enabled: 1
        rpc-authentication-required: 1
        rpc-port: 9091
        rpc-username: transmission
        rpc-password: transmission
        # FIXME: Whitelist the local LAN
        rpc-whitelist-enabled: false
        rpc-whitelist: ""
        upload-limit: 100
        upload-limit-enabled: 0

        blocklist-enabled: false
        blocklist-updates-enabled: true
        blocklist-url: "http://www.example.com/blocklist"

        download-queue-enabled: true
        download-queue-size: 5
        ratio-limit: 2,
        ratio-limit-enabled: true,

        start-added-torrents: true
