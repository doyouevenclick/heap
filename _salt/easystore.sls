/mnt/stor:
  mount.mounted:
    - device: PARTUUID=492634ae-7f81-4558-8ec4-2bc686fef379
    - fstype: auto
    - mkmnt: true
    - opts:
      - defaults
      - nofail  # Do not fail to boot if device fails to mount
