minidlna:
  pkg.installed: []
  service.running:
    - enable: true

/var/cache/minidlna:
  file.directory:
    - user: minidlna

/etc/minidlna.conf:
  file.managed:
    - source: salt://minidlna.conf
    - requires:
      - file: /var/cache/minidlna
    - listen_in:
      - service: minidlna
